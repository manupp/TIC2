// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// || Plantilla para un programa gr'afico.                                ||
// || Puedes descargar la 'ultima versi'on de este proyecto aqu'i:        ||
// || https://github.com/tic-el-prado/BasixTestProject/archive/master.zip ||
// || Antes de modificar el programa, comp'ilalo y ejec'utalo.            ||
// || Lee los comentarios despacio, y luego prueba a cambiar cosas        ||
// || para escribir tu propio programa.                                   ||
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// || Hay que incluir dos librer'ias: "basix.h" and "ticlib.h".           ||
// || Los archivos necesarios ya est'an incluidos en la carpeta adecuada. ||
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include "basix.h"
#include "ticlib.h"

int main()
{
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// || Crea la variable donde se "guarda" la ventana. Tipo: Window.        ||
	// || Aqu'i puedes cambiar el tamanho y el t'itulo de la ventana.         ||
	// || Los argumentos para construir la ventana son:                       ||
	// || (ancho en p'ixeles, alto en p'ixeles, t'itulo)                      ||
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	Window my_window{ 800, 500, "El astronauta" };

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// || Crea aqu'i los objetos gr'aficos (texto, sprites, formas).          ||
	// || Debes crear una variable por cada objeto,                           ||
	// || y luego puedes cambiar sus propiedades (posici'on, color,...)       ||
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	Sprite my_astronaut{ "astronaut.png" };
	my_astronaut.move(400, 250);

	Sprite background{ "universe.jpg" };
	background.move(400, 250);
	
	Text my_text{ "", 64 };
	my_text.setFillColor(Color(255, 50, 50));

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// || Anhade cualquier otro c'odigo que necesite tu programa.             ||
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	int score{ 0 };
	double speed{ 20.0 };
	double direction{ 0.0 };
	bool clicked = false;
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// || Aqu'i empieza el bucle que dibuja la ventana en la pantalla.        ||
	// || Se repite mientras la ventana est'e abierta.                        ||
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	while (my_window.isOpen()) {
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Aqu'i empieza el bucle de procesado de eventos.                     ||
		// || En cada iteraci'on se toma un evento de la cola de eventos,         ||
		// || se guarda el evento en la variable "event",                         ||
		// || y se procesa el evento seg'un su tipo.                              ||
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		Event event;
		while (my_window.pollEvent(event)) {
			if (event.type == Event::Closed) {
				my_window.close();
			}

			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			// || Para cada evento que quieras procesar, incluye aqu'i el c'odigo.    ||
			// || Primero comprueba el tipo (p. ej., "KeyPressed"),                   ||
			// || Y luego el evento concreto (p. ej., "code == Escape").              ||
			// || Finalmente, escribe el c'odigo que quieras ejecutar cuando se       ||
			// || capture este evento.                                                ||
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			else if (event.type == Event::KeyPressed) {
				if (event.key.code == Keyboard::Escape) {
					my_window.close();
				}
			}
		}

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Puedes comprobar el estado del teclado y del rat'on sin necesidad   ||
		// || de capturar el evento correspondiente.                              ||
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		if (Mouse::isButtonPressed(Mouse::Left) && !clicked) {
			Point cursor{ Mouse::getPosition(my_window) };
			if (my_astronaut.contains(cursor)) {

				score++;
				clicked = true;
			}
		}
		else if (!Mouse::isButtonPressed(Mouse::Left)) {
			clicked = false;
		}

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Cada vez que se ejecuta el bucle, se dibuja un "fotograma".         ||
		// || Aqu'i tienes que escribir las sentencias necesarias para            ||
		// || cambiar los objetos de un fotograma al siguiente.                   ||
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		
		my_text.setString("Score: " + to_string(score));

		if (clicked) {
			my_astronaut.setColor(Color(255, 0, 0));
		}
		else {
			my_astronaut.setColor(Color(255, 255, 255));
		}

		double ran{ random_double() };
		if (ran > 0.85) {
			direction += 5;
		}
		else if (ran < 0.15) {
			direction -= 5;
		}
		Point disp{ speed * cos(direction), speed * sin(direction) };
		Point new_position{ my_astronaut.getPosition() + disp };
		if (my_window.contains(new_position, 50, 50)) {
			my_astronaut.setPosition(new_position);
		}

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Esta sentencia es imprescindible: borra el fotograma anterior.      ||
		// || Puedes cambiar el color de fondo de la ventana.                     ||
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		my_window.clear(Color(0, 0, 0));

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Las siguientes sentencias dibujan los objetos en la ventana.        ||
		// || Solo puedes dibujar los objetos creados anteriormente.              ||
		// || Lo que no dibujes, no aparecer'a (recuerda que borraste el frame).  ||
		// || Los objetos se dibujan en el orden de las sentencias:               ||
		// || los 'ultimos se superponen a los anteriores.
		// || into account to control which one is on top of which.               ||
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		my_window.draw(background);
		my_window.draw(my_text);
		my_window.draw(my_astronaut);

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Esta sentencia es imprescindible.                                   ||
		// || Env'ia el buffer a la ventana.                                      ||
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		my_window.display();

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Esta sentencia puede ser conveniente para no consumir mucha CPU.    ||
		// || Se detiene la ejecuci'on durante los milisegundos especificados.    ||
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		wait_for(10);
	}
}