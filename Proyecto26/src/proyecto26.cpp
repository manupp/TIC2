// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// || Plantilla para un programa gr'afico.                                ||
// || Antes de modificar el programa, comp'ilalo y ejec'utalo.            ||
// || Lee los comentarios despacio, y luego prueba a cambiar cosas        ||
// || para escribir tu propio programa.                                   ||
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// || Hay que incluir dos librer'ias: "basix.h" and "ticlib.h".           ||
// || Los archivos necesarios ya est'an incluidos en la carpeta adecuada. ||
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include "basix.h"
#include "ticlib.h"

int main()
{
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// || Crea la variable donde se "guarda" la ventana. Tipo: Window.        ||
	// || Aqu'i puedes cambiar el tamanho y el t'itulo de la ventana.         ||
	// || Los argumentos para construir la ventana son:                       ||
	// || (ancho en p'ixeles, alto en p'ixeles, t'itulo)                      ||
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	Window my_window{ 800, 500, "Tanques" };

	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// || Crea aqu'i los objetos gr'aficos (texto, sprites, formas).          ||
	// || Debes crear una variable por cada objeto,                           ||
	// || y luego puedes cambiar sus propiedades (posici'on, color,...)       ||
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// || Anhade cualquier otro c'odigo que necesite tu programa.             ||
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// || Aqu'i empieza el bucle que dibuja la ventana en la pantalla.        ||
	// || Se repite mientras la ventana est'e abierta.                        ||
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	while (my_window.isOpen()) {
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Aqu'i empieza el bucle de procesado de eventos.                     ||
		// || En cada iteraci'on se toma un evento de la cola de eventos,         ||
		// || se guarda el evento en la variable "event",                         ||
		// || y se procesa el evento seg'un su tipo.                              ||
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		Event event;
		while (my_window.pollEvent(event)) {
			if (event.type == Event::Closed) {
				my_window.close();
			}

			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			// || Para cada evento que quieras procesar, incluye aqu'i el c'odigo.    ||
			// || Primero comprueba el tipo (p. ej., "KeyPressed"),                   ||
			// || Y luego el evento concreto (p. ej., "code == Escape").              ||
			// || Finalmente, escribe el c'odigo que quieras ejecutar cuando se       ||
			// || capture este evento.                                                ||
			// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			else if (event.type == Event::KeyPressed) {
				if (event.key.code == Keyboard::Escape) {
					my_window.close();
				}
				else if (event.key.code == Keyboard::Left) {

				}
			}
		}
		
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Cada vez que se ejecuta el bucle, se dibuja un "fotograma".         ||
		// || Aqu'i tienes que escribir las sentencias necesarias para            ||
		// || cambiar los objetos de un fotograma al siguiente.                   ||
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		
		
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Esta sentencia es imprescindible: borra el fotograma anterior.      ||
		// || Puedes cambiar el color de fondo de la ventana.                     ||
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		my_window.clear(Color(0, 0, 0));

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Las siguientes sentencias dibujan los objetos en la ventana:        ||
		// || window.draw(objeto);                                                ||
		// || Solo puedes dibujar los objetos creados anteriormente.              ||
		// || Lo que no dibujes, no aparecer'a (recuerda que borraste el frame).  ||
		// || Los objetos se dibujan en el orden de las sentencias:               ||
		// || los 'ultimos se superponen a los anteriores.
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Esta sentencia es imprescindible.                                   ||
		// || Env'ia el buffer a la ventana.                                      ||
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		my_window.display();

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// || Esta sentencia puede ser conveniente para no consumir mucha CPU.    ||
		// || Se detiene la ejecuci'on durante los milisegundos especificados.    ||
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		wait_for(10);
	}
}