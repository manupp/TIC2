#ifndef ezsfml_hpp
#define ezsfml_hpp

#include <iostream>

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <algorithm>
#include <assert.h>

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>

using namespace std;

namespace basix
{

  // Avoid namespace prefixing and other renamings
  using Color = sf::Color;
  using Event = sf::Event;
  using Font = sf::Font;
  using Keyboard = sf::Keyboard;
  using Mouse = sf::Mouse;
  using Sound = sf::Sound;
  using Texture = sf::Texture;
  using Vector = sf::Vector2<double>;
  
  // Resource holders

  template<typename Resource, typename Identifier>
  class Resource_holder
  {
  public:
    void load(Identifier id, const std::string& filename);
    Resource& get(Identifier id);
    const Resource& get(Identifier id) const;
  private:
    std::map<Identifier, std::unique_ptr<Resource>> _resource_map;
  };

  template<typename Resource, typename Identifier>
  inline void Resource_holder<Resource, Identifier>::load(Identifier id,
                                                          const std::string& filename)
  {
    std::unique_ptr<Resource> resource(new Resource());
    if (!resource->loadFromFile(filename))
      throw std::runtime_error("Failed to load " + filename);
    auto inserted = _resource_map.insert(std::make_pair(id, std::move(resource)));
  }

  template<typename Resource, typename Identifier>
  inline Resource& Resource_holder<Resource, Identifier>::get(Identifier id)
  {
    auto found = _resource_map.find(id);
    assert(found != _resource_map.end());
    return *found->second;
  }

  template<typename Resource, typename Identifier>
  inline const Resource& Resource_holder<Resource, Identifier>::get(Identifier id) const
  {
    auto found = _resource_map.find(id);
    assert(found != _resource_map.end());
    return *found->second;
  }

  using Identifier = std::string;

  Resource_holder<Texture, Identifier> texture_holder {};
  Resource_holder<Font, Identifier> font_holder {};
  Resource_holder<sf::SoundBuffer, Identifier> sound_buffer_holder {};
  
  // Window class
  
  class Point : public sf::Vector2f {
  public:
	  Point(const double x, const double y);
	  Point(const sf::Vector2f vf);
	  Point(const sf::Vector2i vi);
  };

  Point::Point(const double x, const double y)
	  :sf::Vector2f(float(x), float(y))
  {}

  Point::Point(const sf::Vector2f v)
	  : sf::Vector2f(v)
  {}

  Point::Point(const sf::Vector2i vi)
	  : sf::Vector2f(vi)
  {}

  double distance(const Point a, const Point b)
  {
	  return sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
  }

  class Window : public sf::RenderWindow
  {
  public:

    Window(const int width, const int height, const std::string& title);
    void clear(const Color& color = Color(255, 255, 255, 0));
	bool contains(const Point& point, 
		const int hmargin = 0, const int vmargin = 0) const;
	bool contains(const double x, const double y, 
		const int hmargin = 0, const int vmargin = 0) const;
 
  };

  const int DEFAULT_WINDOW_WIDTH = 800;
  const int DEFAULT_WINDOW_HEIGHT = 600;

  inline Window::Window(const int width = DEFAULT_WINDOW_WIDTH,
                        const int height = DEFAULT_WINDOW_HEIGHT,
                        const std::string& title = "")
    :sf::RenderWindow(sf::VideoMode(width, height), title)
  { }

  inline void Window::clear(const Color& color)
  {
    sf::RenderWindow::clear(color);
  }

  bool Window::contains(const Point& point,
	  const int hmargin,
	  const int vmargin) const
  {
	  Vector window_size{ getSize() };
	  return point.x > hmargin && point.x < window_size.x - hmargin
		  && point.y > vmargin && point.y < window_size.y - vmargin;
  }

  bool Window::contains(const double x, const double y,
	  const int hmargin, const int vmargin) const
  {
	  return contains(Point{ x, y }, hmargin, vmargin);
  }

  // Classes and functions to add graphic elements (sprites, circles, boxes, text,...)

 
  class Sprite : public sf::Sprite {
  public:
    Sprite(const std::string& filename);
	bool contains(const Point point) const;
	bool contains(const double x, const double y) const;
  };

  Sprite::Sprite(const std::string& filename)
  {
    texture_holder.load(filename, filename);
    setTexture(texture_holder.get(filename), true);
    setOrigin(getLocalBounds().width/2,
              getLocalBounds().height/2);
  }

  bool Sprite::contains(const Point point) const
  {
	  return getGlobalBounds().contains(point);
  }

  bool Sprite::contains(const double x, const double y) const
  {
	  return contains(Point{ x, y });
  }
  
  inline Sprite create_sprite(const std::string& filename)
  {
    return Sprite(filename);
  }

  // Overload for backwards compatibility. First argument unused.
  inline Sprite create_sprite(Window&, const std::string& filename)
  {
    return create_sprite(filename);
  }
  
  const std::string DEFAULT_FONT = "UbuntuMono-R.ttf";
  const Identifier DEFAULT_FONT_ID = "default";
  const int DEFAULT_FONT_SIZE = 16;
  const Color DEFAULT_TEXT_COLOR = Color(0, 0, 0);

  class Text : public sf::Text {
  public:
    Text(const std::string& text_string = "",
         const int font_size = DEFAULT_FONT_SIZE,
         const Identifier font = DEFAULT_FONT);
	bool contains(const Point point) const;
	bool contains(const double x, const double y) const;
  };

  Text::Text(const std::string& text_string,
             const int font_size,
             const Identifier font)
  {
    setString(text_string);
    font_holder.load(font, font);
    setFont(font_holder.get(font));
    setCharacterSize(font_size);
    setFillColor(DEFAULT_TEXT_COLOR);
  }

  bool Text::contains(const Point point) const
  {
	  return getGlobalBounds().contains(point);
  }

  bool Text::contains(const double x, const double y) const
  {
	  return contains(Point{ x, y });
  }


  inline Text create_text(const std::string& text_string = "",
                          const int font_size = DEFAULT_FONT_SIZE,
                          const Identifier font = DEFAULT_FONT)
  {
    Text text(text_string, font_size, font);
    return text;
  }
  
  // Overload for backwards compatibility. First argument unused.
  inline Text create_text(Window&,
                              const std::string& text_string = "",
                              const int font_size = DEFAULT_FONT_SIZE,
                              const Identifier font = DEFAULT_FONT)
  {
    return create_text(text_string, font_size, font);
  }

  inline void wait_for(const int ms) {
    sf::sleep(sf::milliseconds(ms));
  }
  
  inline Sound create_sound(const std::string& filename)
  {
    sound_buffer_holder.load(filename, filename);
    Sound new_sound;
    new_sound.setBuffer(sound_buffer_holder.get(filename));
    return new_sound;
  }

  // Overload for backwards compatibility. First argument unused.
  inline Sound create_sound(Window&, const std::string& filename)
  {
    return create_sound(filename);
  }

  class Circle : public sf::CircleShape {
  public:
	  Circle(const double radius, const Point center);
	  bool contains(const Point& point) const;
	  bool contains(const double x, const double y) const;
  };

  Circle::Circle(const double radius, const Point center)
	  : sf::CircleShape(float(radius))
  {
	  setOrigin(Point{ radius, radius });
	  move(center);
  }

  bool Circle::contains(const Point& point) const
  {
	  return distance(point, getPosition()) < getRadius();
  }

  bool Circle::contains(const double x, const double y) const
  {
	  return contains(Point{ x, y });
  }

  inline Circle create_circle(const double radius, const Point center)
  {
    return Circle(radius, center);
  }

  // Overload for backwards compatibility. First argument unused.
  inline Circle create_circle(Window&, const double radius, const Point center)
  {
    return create_circle(radius, center);
  }

  class Rectangle : public sf::RectangleShape {
  public:
	  Rectangle(const double width, const double height, const Point center);
  };

  Rectangle::Rectangle(const double width, const double height, const Point center)
	  : sf::RectangleShape(sf::Vector2f{ float(width), float(height) })
  {
	  setOrigin(Point{ width / 2, height / 2 });
	  move(center);
  }

  inline Rectangle create_rectangle(const double width,
                                    const double height,
                                    const Point center)
  {
	  return Rectangle{ width, height, center };
  }

  // Overload for backwards compatibility. First argument unused.
  inline Rectangle create_rectangle(Window&,
                                    const double width,
                                    const double height,
                                    const Point center)
  {
    return create_rectangle(width, height, center);
  }

  bool is_inside_window(const Point point,
	  const Window& window,
	  int hmargin = 0,
	  int vmargin = 0)
  {
	  Vector window_size{ window.getSize() };
	  return point.x > hmargin && point.x < window_size.x - hmargin
		  && point.y > vmargin && point.y < window_size.y - vmargin;
  }

  bool is_inside_rectangle(const Point point, const Point topleft, const Point bottomright)
  {
	  return point.x > topleft.x
		  && point.x < bottomright.x
		  && point.y > topleft.y
		  && point.y < bottomright.y;
  }

  bool is_inside_rectangle(const Point point, const Rectangle& rectangle)
  {
	  return rectangle.getGlobalBounds().contains(point);
  }

}

using namespace basix;

#endif
